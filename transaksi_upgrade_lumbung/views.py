from datetime import datetime
from urllib import request
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

# admin: R, user: CR

SET_SEARCH_PATH = 'set search_path to hiday'

def create_transaksi_upgrade_lumbung(request):
    if 'email' not in request.session:
        return redirect('/login/')
    if request.session['tipe'] != 'Pengguna':
        return redirect('/home/')

    prefilled_data = get_lumbung_by_pk(request.session['email'])

    if (prefilled_data != None):
        lumbung_level = prefilled_data.level
        lumbung_next_level = lumbung_level + 1

        lumbung_k_maks = prefilled_data.kapasitas_maksimal
        lumbung_next_k_maks = lumbung_k_maks + 50

        response = {
            'lumbung_level': lumbung_level,
            'lumbung_next_level': lumbung_next_level,
            'lumbung_k_maks': lumbung_k_maks,
            'lumbung_next_k_maks': lumbung_next_k_maks,
        }
        response['error']=False
 
    if(request.method == "POST"):
        print(request.POST)
        
        email = request.session['email']
        waktu = datetime.now()
        level = lumbung_next_level
        kapasitas_maksimal = lumbung_next_k_maks

        # if (get_koin(request.session['email']).koin >= 200):
        insert_db(email, waktu)
        update_lumbung(email, level, kapasitas_maksimal)
        print('Berhasil mengupgrade lumbung')

        return redirect("read_transaksi_upgrade_lumbung")
        # else:
        #     print('Koin tidak mencukupi')
 
    return render(request, "create_transaksi_upgrade_lumbung.html", response)

def read_transaksi_upgrade_lumbung(request):
    if 'email' not in request.session:
        return redirect('/login/')

    if (request.session['tipe'] == 'Admin'):

        response = {
            'transaksis': get_db(),
        }

        return render(request, 'read_transaksi_upgrade_lumbung_admin.html', response)

    elif (request.session['tipe'] == 'Pengguna'):

        response = {
            'transaksis': get_db_for_pengguna(request.session['email']),
        }

        return render(request, 'read_transaksi_upgrade_lumbung_pengguna.html', response)

    else:
        return redirect('/login/')


def insert_db(email, waktu):
    query = f"""
    INSERT INTO transaksi_upgrade_lumbung
    (email, waktu)
    VALUES ('{email}', '{waktu}');
    """

    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

def update_lumbung(email, level, kapasitas_maksimal):
    query = f"""
    UPDATE lumbung
    SET level = {level},
      kapasitas_maksimal = {kapasitas_maksimal}
    WHERE email='{email}';
    """

    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

def get_db():
    query = """
    SELECT * FROM transaksi_upgrade_lumbung;
    """
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def get_lumbung_by_pk(email):
    query = f"""
    SELECT * FROM lumbung
    WHERE email='{email}';
    """
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)[0]
    
def get_koin(email):
    query = f"""
    SELECT koin FROM pengguna
    WHERE email='{email}';
    """
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)[0]

def get_db_for_pengguna(email):
    query = f"""
    SELECT * FROM transaksi_upgrade_lumbung where email = '{email}';
    """
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def __namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]