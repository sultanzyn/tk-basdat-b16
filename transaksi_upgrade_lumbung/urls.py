from django.conf.urls import url
from django.urls import include,path
from . import views

urlpatterns = [
    path("upgrade/", views.create_transaksi_upgrade_lumbung, name="create_transaksi_upgrade_lumbung"),
    path("read/", views.read_transaksi_upgrade_lumbung, name="read_transaksi_upgrade_lumbung"),
] 