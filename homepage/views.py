from urllib import request
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

def pengguna(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT * FROM PENGGUNA
            WHERE email = '{email}'""") 
        row = cursor.fetchall()
        password = row[0][1]
        nama_area = row[0][2]
        xp = row[0][3]
        koin = row[0][4]
        level = row[0][5]
        context = {
            'email': email,
            'password': password,
            'nama_area': nama_area,
            'xp': xp,
            'koin': koin,
            'level': level,
        }
    return render(request, 'pengguna_home.html', context) 


def admin(request):
    context = {"session" : request.session}
    return render(request, 'admin_home.html', context) 

def logout(request):
    # Hilangkan semua dari session
    request.session.flush()

    return redirect("/")

def lumbung_pengguna(request):
    return redirect("/lumbung/pengguna")

def lumbung_admin(request):
    return redirect("/lumbung/admin")    