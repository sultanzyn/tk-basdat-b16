from django.urls import path
from . import views

urlpatterns = [
    path('pengguna/', views.pengguna),
    path('admin/', views.admin),
    path('pengguna/logout/', views.logout),
    path('admin/logout/', views.logout),
    path('pengguna/lumbung/', views.lumbung_pengguna),
    path('admin/lumbung/', views.lumbung_admin),
]