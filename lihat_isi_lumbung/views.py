from copy import error
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.contrib import messages

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def lumbung_admin(request):

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    # hasil panen
    cursor.execute("select p.id, p.nama, p.harga_jual, p.sifat_produk, l.email from produk p, hasil_panen hp, lumbung_memiliki_produk lmp, lumbung l where p.id = hp.id_produk and p.id = lmp.id_produk and lmp.id_lumbung = l.email")
    hasil_panen = namedtuplefetchall(cursor)

    # produk hewan
    cursor.execute("select p.id, p.nama, p.harga_jual, p.sifat_produk, l.email from produk p, produk_hewan ph, lumbung_memiliki_produk lmp, lumbung l where p.id = ph.id_produk and p.id = lmp.id_produk and lmp.id_lumbung = l.email")
    produk_hewan = namedtuplefetchall(cursor)

    # produk makanan
    cursor.execute("select p.id, p.nama, p.harga_jual, p.sifat_produk, l.email from produk p, produk_makanan pm, lumbung_memiliki_produk lmp, lumbung l where p.id = pm.id_produk and p.id = lmp.id_produk and lmp.id_lumbung = l.email")
    produk_makanan = namedtuplefetchall(cursor)

    return render(request, 'lumbung_admin.html', {'hasil_panen': hasil_panen, 'produk_hewan': produk_hewan, 'produk_makanan': produk_makanan}) 

def lumbung_pengguna(request):

    email = request.session['email']
    
    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    #ambil informasi lumbung
    cursor.execute(f"select level, kapasitas_maksimal, total from lumbung where email = '{request.session['email']}'")
    lumbung = namedtuplefetchall(cursor)[0]

    # hasil panen
    cursor.execute(f"""select p.id, p.nama, p.harga_jual, p.sifat_produk, l.email from produk p, hasil_panen hp, lumbung_memiliki_produk lmp, lumbung l where p.id = hp.id_produk and p.id = lmp.id_produk and lmp.id_lumbung = l.email and l.email = '{email}'""")
    hasil_panen = namedtuplefetchall(cursor)

    # produk hewan
    cursor.execute(f"""select p.id, p.nama, p.harga_jual, p.sifat_produk, l.email from produk p, produk_hewan ph, lumbung_memiliki_produk lmp, lumbung l where p.id = ph.id_produk and p.id = lmp.id_produk and lmp.id_lumbung = l.email and l.email = '{email}'""")
    produk_hewan = namedtuplefetchall(cursor)

    # produk makanan
    cursor.execute(f"""select p.id, p.nama, p.harga_jual, p.sifat_produk, l.email from produk p, produk_makanan pm, lumbung_memiliki_produk lmp, lumbung l where p.id = pm.id_produk and p.id = lmp.id_produk and lmp.id_lumbung = l.email and l.email = '{email}'""")
    produk_makanan = namedtuplefetchall(cursor)

    return render(request, 'lumbung_pengguna.html', {'hasil_panen': hasil_panen, 'produk_hewan': produk_hewan, 'produk_makanan': produk_makanan, 'lumbung': lumbung})

def logout(request):
    request.session.flush()
    return redirect('/')

def home_pengguna(request):
    return redirect("/home/pengguna")

def home_admin(request):
    return redirect("/home/admin")    