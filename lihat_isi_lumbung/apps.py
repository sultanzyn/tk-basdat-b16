from django.apps import AppConfig


class LihatIsiLumbungConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lihat_isi_lumbung'
