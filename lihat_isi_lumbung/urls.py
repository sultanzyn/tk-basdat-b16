from django.urls import path
from . import views

urlpatterns = [
    path('pengguna/', views.lumbung_pengguna),
    path('admin/', views.lumbung_admin),
    path('pengguna/logout/', views.logout),
    path('admin/logout/', views.logout),
    path('pengguna/home/', views.home_pengguna),
    path('admin/home/', views.home_admin)
]