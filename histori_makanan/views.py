from django.shortcuts import render
from collections import namedtuple
from django.db import connection

# Create your views here.
def histori_makanan_read_pengguna(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT hm.email as email, hp.waktu_awal as waktu_awal, hp.waktu_selesai as waktu_selesai, hp.jumlah as jumlah, hp.xp as xp, hm.id_produk_makanan as id, hm.id_alat_produksi as alat  
            FROM histori_produksi_makanan hm inner join histori_produksi hp on hm.email = hp.email
            where hm.email = '{email}'""") 
        row = namedtuplefetchall(cursor)
        context = {
            'data' : row,
        }
    return render(request, 'pengguna_read.html', context)

def histori_makanan_read_admin(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT hm.email as email, hp.waktu_awal as waktu_awal, hp.waktu_selesai as waktu_selesai, hp.jumlah as jumlah, hp.xp as xp, hm.id_produk_makanan as id, hm.id_alat_produksi as alat  
            FROM histori_produksi_makanan hm inner join histori_produksi hp on hm.email = hp.email""") 
        row = namedtuplefetchall(cursor)
        context = {
            'data' : row,
        }
    return render(request, 'admin_read.html', context)

def produksi_create(request):
    return render(request, 'produksi_create_form.html')

def produksi_update(request):
    return render(request, 'produksi_update_form.html')

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]