from django.apps import AppConfig


class HistoriMakananConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'histori_makanan'
