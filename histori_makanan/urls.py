from django.urls import path
from . import views

urlpatterns = [
    path('admin/read/', views.histori_makanan_read_admin, name='read_admin'),
    path('pengguna/read/', views.histori_makanan_read_pengguna, name='read_pengguna'),
]
