from ast import arguments
from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple

# Create your views here.
def aset(request):
    role = request.session["tipe"]
    print(role)
    argument = {
        'role' : role
    }
    return render(request, 'aset.html', argument)

def buat_aset(request):
    role = request.session["tipe"]
    print(role)
    argument = {
        'role' : role
    }
    return render(request, 'buat_aset.html', argument)

def form_buat_aset(request, jenis_aset):
    # print(jenis_aset)
    role = request.session["tipe"]
    # print(role)
    table = jenis_aset.replace(" ", "_")
    id = 0
    # print(table)
    jenis_tanaman = ''

    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        cursor.execute(f"select count(*) as count from {table}")
        id = namedtuplefetchall(cursor)
        # print(id[0].count)
        cursor.execute(f"select nama from aset a, bibit_tanaman bt where a.ID = bt.ID_Aset;")
        jenis_tanaman = namedtuplefetchall(cursor)
        # print(jenis_tanaman)

    id = format(id[0].count + 1, "03d")
    # print(id)
    
    argument = {
        'role' : role,
        'jenis_aset' : jenis_aset,
        'id' : id,
        'jenis_tanaman' : jenis_tanaman
    }


    if (request.method == "POST"):
        nama = request.POST["Nama"]
        min_level = request.POST["Minimum Level"]
        hrg_beli = request.POST["Harga Beli"]
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay;")
            try:
                if (jenis_aset == "Dekorasi"):
                    hrg_jual = request.POST["Harga Jual"]
                    cursor.execute(f"insert into aset values('DK{id}', '{nama}', {min_level}, {hrg_beli});")
                    cursor.execute(f"insert into dekorasi values('DK{id}', {hrg_jual});")

                if (jenis_aset == "Bibit Tanaman"):
                    drs_panen = request.POST["Durasi Panen"]
                    cursor.execute(f"insert into aset values('BT{id}', '{nama}', {min_level}, {hrg_beli});")
                    cursor.execute(f"insert into bibit_tanaman values('BT{id}', '{drs_panen}');")
                    # drs_panen time

                if (jenis_aset == "Kandang"):
                    kps_maks = request.POST["Kapasitas Maks"]
                    jns_hwn = request.POST["Jenis Hewan"]
                    cursor.execute(f"insert into aset values('KD{id}', '{nama}', {min_level}, {hrg_beli});")
                    cursor.execute(f"insert into kandang values('KD{id}', {kps_maks}, '{jns_hwn}');")

                if (jenis_aset == "Hewan"):

                    print("Hewan")
                    cursor.execute(f"select id_aset from kandang where jenis_hewan = '{nama}';")
                    id_kandang = cursor.fetchone()
                    print(id_kandang)
                    if id_kandang != None:
                        id_kandang = id_kandang[0] 
                        drs_pdk = request.POST["Durasi Produksi"]
                        cursor.execute(f"insert into aset values('HW{id}', '{nama}', {min_level}, {hrg_beli});")
                        cursor.execute(f"insert into hewan values('HW{id}', '{drs_pdk}', '{id_kandang}');")
                    else:
                        print('redirect')
                        return redirect("/aset/error-message/Data untuk kandang hewan tersebut belum ada, silahkan membuat kandang terlebih dahulu")
                    # drs_pdk time

                if (jenis_aset == "Alat Produksi"):
                    kps_maks = request.POST["Kapasitas Maks"]
                    cursor.execute(f"insert into aset values('AP{id}', '{nama}', {min_level}, {hrg_beli});")
                    cursor.execute(f"insert into alat_produksi values('AP{id}', {kps_maks});")

                if (jenis_aset == "Petak Sawah"):
                    jns_tnm = request.POST["Jenis Tanaman"]
                    cursor.execute(f"insert into aset values('PS{id}', '{nama}', {min_level}, {hrg_beli});")
                    cursor.execute(f"insert into petak_sawah values('PS{id}', '{jns_tnm}');")
            except:
                if (jenis_aset == "Dekorasi"):
                    cursor.execute(f"delete from aset where id='DK{id}';")

                if (jenis_aset == "Bibit Tanaman"):
                    cursor.execute(f"delete from aset where id='BT{id}';")

                if (jenis_aset == "Kandang"):
                    cursor.execute(f"delete from aset where id='KD{id}';")

                if (jenis_aset == "Hewan"):
                    cursor.execute(f"delete from aset where id='HW{id}';")

                if (jenis_aset == "Alat Produksi"):
                    cursor.execute(f"delete from aset where id='AP{id}';")

                if (jenis_aset == "Petak Sawah"):
                    cursor.execute(f"delete from aset where id='PS{id}';")

        return redirect(f"/aset/list-aset/tabel/{jenis_aset}")

    return render(request, 'form_buat_aset.html', argument)

def list_aset(request):
    role = request.session["tipe"]
    print(role)
    argument = {
        'role' : role
    }
    return render(request, 'list_aset.html', argument)

def tabel_aset(request, jenis_aset):
    print(jenis_aset)
    role = request.session["tipe"]
    print(role)
    rows = ""
    table = jenis_aset.replace(" ", "_")
    deletables = ""

    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        cursor.execute(f"select * from aset a, {table} d where a.ID = d.ID_Aset;")
        rows = namedtuplefetchall(cursor)
        cursor.execute(f"""
            select id
            from aset
            where id not in
            (
            select ID_Bibit_Tanaman as ID
            from BIBIT_TANAMAN_MENGHASILKAN_HASIL_PANEN 
            union
            select ID_Produk_Hewan as ID
            from HEWAN_MENGHASILKAN_PRODUK_HEWAN
            union
            select ID_Alat_Produksi as ID
            from PRODUKSI
            union
            select ID_Aset as ID
            from KOLEKSI_ASET_MEMILIKI_ASET
            union
            select ID_Bibit_Tanaman as ID
            from HISTORI_TANAMAN
            union
            select ID_Hewan as ID
            from HISTORI_HEWAN
            );
        """)
        deletables = cursor.fetchall()
        deletables = [deletable[0] for deletable in deletables]
        print(deletables)


    argument = {
        'role' : role,
        'jenis_aset' : jenis_aset,
        'rows' : rows,
        'deletables' : deletables
    }
    return render(request, 'tabel_aset.html', argument)

def update_aset(request, jenis_aset, id):

    role = request.session["tipe"]
    table = jenis_aset.replace(" ", "_")
    jenis_tanaman = ""

    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        cursor.execute(f"select * from aset a, {table} d where a.ID = '{id}' AND a.ID = d.ID_Aset;")
        row = namedtuplefetchall(cursor)
        print(row)
        print(row[0])
        cursor.execute(f"select nama from aset a, bibit_tanaman bt where a.ID = bt.ID_Aset;")
        jenis_tanaman = namedtuplefetchall(cursor)

    argument = {
        'role' : role,
        'jenis_aset' : jenis_aset,
        'row' : row[0],
        'jenis_tanaman' : jenis_tanaman
    }

    if (request.method == "POST"):
        min_level = request.POST["Minimum Level"]
        hrg_beli = request.POST["Harga Beli"]

        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay;")
            try:
                if (jenis_aset == "Dekorasi"):
                    hrg_jual = request.POST["Harga Jual"]
                    cursor.execute(f"update aset set minimum_level = {min_level}, harga_beli = {hrg_beli} where ID = '{id}';")
                    cursor.execute(f"update dekorasi set harga_jual = {hrg_jual} where ID_Aset = '{id}';")

                if (jenis_aset == "Bibit Tanaman"):
                    drs_panen = request.POST["Durasi Panen"]
                    cursor.execute(f"update aset set minimum_level = {min_level}, harga_beli = {hrg_beli} where ID = '{id}';")
                    cursor.execute(f"update bibit_tanaman set durasi_panen = '{drs_panen}' where ID_Aset = '{id}';")
                    # drs_panen time

                if (jenis_aset == "Kandang"):
                    kps_maks = request.POST["Kapasitas Maks"]
                    cursor.execute(f"update aset set minimum_level = {min_level}, harga_beli = {hrg_beli} where ID = '{id}';")
                    cursor.execute(f"update kandang set kapasitas_maks = {kps_maks} where ID_Aset = '{id}';")

                if (jenis_aset == "Hewan"):
                    drs_pdk = request.POST["Durasi Produksi"]
                    cursor.execute(f"update aset set minimum_level = {min_level}, harga_beli = {hrg_beli} where ID = '{id}';")
                    cursor.execute(f"update hewan set durasi_produksi = {drs_pdk} where ID_Aset = '{id}';")
                    # drs_pdk time

                if (jenis_aset == "Alat Produksi"):
                    kps_maks = request.POST["Kapasitas Maks"]
                    cursor.execute(f"update aset set minimum_level = {min_level}, harga_beli = {hrg_beli} where ID = '{id}';")
                    cursor.execute(f"update alat_produksi set kapasitas_maks = {kps_maks} where ID_Aset = '{id}';")

                if (jenis_aset == "Petak Sawah"):
                    jns_tnm = request.POST["Jenis Tanaman"]
                    cursor.execute(f"update aset set minimum_level = {min_level}, harga_beli = {hrg_beli} where ID = '{id}';")
                    cursor.execute(f"update petak_sawah set jenis_tanaman = {jns_tnm} where ID_Aset = '{id}';")
            except:
                print("error update")
        
        return redirect(f"/aset/list-aset/tabel/{jenis_aset}")

   
    return render(request, 'form_update_aset.html', argument)

def delete_aset(request, jenis_aset, id):
    print(id)

    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        cursor.execute(f"delete from aset where id = '{id}';")

    return redirect(f"/aset/list-aset/tabel/{jenis_aset}")

def error_message(request, message):
    arguments = {
        "message" : message
    }
    return render(request, 'error_message.html', arguments)

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]