from django.urls import path
from . import views

urlpatterns = [
    path('', views.aset),
    path('error-message/<message>', views.error_message),
    path('buat-aset', views.buat_aset),
    path('buat-aset/form/<jenis_aset>', views.form_buat_aset),
    path('list-aset', views.list_aset),
    path('list-aset/tabel/<jenis_aset>', views.tabel_aset),
    path('update-aset/<jenis_aset>/<id>', views.update_aset),
    path('delete-aset/<jenis_aset>/<id>', views.delete_aset)
]