from django.apps import AppConfig


class AsetPageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aset_page'
