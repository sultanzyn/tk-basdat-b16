from django.urls import path
from . import views

urlpatterns = [
    path('', views.koleksi_aset),
    path('list-koleksi-aset', views.list_koleksi_aset),
    path('list-koleksi-aset/table/<jenis_aset>', views.table_koleksi_aset),
    path('buat-transaksi-pembelian-aset', views.buat_transaksi_pembelian_aset),
    path('list-transaksi-pembelian-aset', views.list_transaksi_pembelian_aset)
]