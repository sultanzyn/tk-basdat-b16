from ast import arg, arguments
from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
import datetime;

# Create your views here.
def koleksi_aset(request):
    role = request.session["tipe"]

    arguments = {
        'role' : role
    }

    return render(request, 'koleksi_aset.html', arguments)

def list_koleksi_aset(request):
    return render(request, 'list_koleksi_aset.html')

def table_koleksi_aset(request, jenis_aset):

    table = jenis_aset.replace(" ", "_")
    email = request.session['email']
    role = request.session["tipe"]

    print(table, email)
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        cursor.execute(f"""select k.id_koleksi_aset, k.jumlah, a.*, d.* 
        from koleksi_aset_memiliki_aset k, aset a, {table} d 
        where k.id_koleksi_aset = '{email}' 
        AND k.ID_Aset = a.ID AND d.ID_Aset = a.ID;""")

        rows = namedtuplefetchall(cursor)

    print(rows)
    arguments = {
        'rows' : rows,
        'role' : role,
        'jenis_aset' : jenis_aset
    }

    return render(request, 'table_koleksi_aset.html', arguments)

# fitur 10

def buat_transaksi_pembelian_aset(request):

    email = request.session['email']
    role = request.session["tipe"]
    rows = ""

    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        cursor.execute(f"select * from  aset;")
        rows = namedtuplefetchall(cursor)

        cursor.execute(f"select id_aset from dekorasi;")
        dekorasi = cursor.fetchall()
        dekorasi = [aset[0] for aset in dekorasi]

        cursor.execute(f"select id_aset from bibit_tanaman;")
        bibit_tanaman = cursor.fetchall()
        bibit_tanaman = [aset[0] for aset in bibit_tanaman]

        cursor.execute(f"select id_aset from kandang;")
        kandang = cursor.fetchall()
        kandang = [aset[0] for aset in kandang]

        cursor.execute(f"select id_aset from hewan;")
        hewan = cursor.fetchall()
        hewan = [aset[0] for aset in hewan]

        cursor.execute(f"select id_aset from alat_produksi;")
        alat_produksi = cursor.fetchall()
        alat_produksi = [aset[0] for aset in alat_produksi]

        cursor.execute(f"select id_aset from petak_sawah;")
        petak_sawah = cursor.fetchall()
        petak_sawah = [aset[0] for aset in petak_sawah]

    if (request.method == "POST"):
        id_aset = request.POST["Detail Aset"]
        jumlah = request.POST["Jumlah"]
        ct = datetime.datetime.now()
        print(ct)
        with connection.cursor() as cursor:
            print("a")
            cursor.execute("set search_path to HiDay;")
            cursor.execute(f"insert into transaksi_pembelian values('{email}', '{ct}', {jumlah}, '{id_aset}');")

        return redirect(f"/koleksi-aset/list-transaksi-pembelian-aset")

    arguments = {
        'rows' : rows,
        'dekorasi' : dekorasi,
        'bibit_tanaman' : bibit_tanaman,
        'kandang' : kandang,
        'hewan' : hewan,
        'alat_produksi' : alat_produksi,
        'petak_sawah' : petak_sawah
    }

    return render(request, 'buat_transaksi_pembelian_aset.html', arguments)

def list_transaksi_pembelian_aset(request):

    email = request.session['email']
    role = request.session["tipe"]
    rows = ""

    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay;")
        if (role == "Admin"):
            cursor.execute(f"select * from transaksi_pembelian tp, aset a where tp.ID_Aset = a.ID;")
        else:
            cursor.execute(f"select * from transaksi_pembelian tp, aset a where tp.ID_Aset = a.ID AND tp.Email = '{email}';")
        rows = namedtuplefetchall(cursor)

        cursor.execute(f"select id_aset from dekorasi;")
        dekorasi = cursor.fetchall()
        dekorasi = [aset[0] for aset in dekorasi]

        cursor.execute(f"select id_aset from bibit_tanaman;")
        bibit_tanaman = cursor.fetchall()
        bibit_tanaman = [aset[0] for aset in bibit_tanaman]

        cursor.execute(f"select id_aset from kandang;")
        kandang = cursor.fetchall()
        kandang = [aset[0] for aset in kandang]

        cursor.execute(f"select id_aset from hewan;")
        hewan = cursor.fetchall()
        hewan = [aset[0] for aset in hewan]

        cursor.execute(f"select id_aset from alat_produksi;")
        alat_produksi = cursor.fetchall()
        alat_produksi = [aset[0] for aset in alat_produksi]

        cursor.execute(f"select id_aset from petak_sawah;")
        petak_sawah = cursor.fetchall()
        petak_sawah = [aset[0] for aset in petak_sawah]




    # print(rows)
    arguments = {
        'rows' : rows,
        'dekorasi' : dekorasi,
        'bibit_tanaman' : bibit_tanaman,
        'kandang' : kandang,
        'hewan' : hewan,
        'alat_produksi' : alat_produksi,
        'petak_sawah' : petak_sawah
    }

    return render(request, 'list_transaksi_pembelian_aset.html', arguments)

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
