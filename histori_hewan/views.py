from copy import error
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from urllib import request

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def histori_hewan_read_pengguna(request):

    email = request.session['email']

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    cursor.execute(f"""select hp.email, hp.waktu_awal, hp.waktu_selesai, hp.jumlah, hp.xp, a.nama 
            from histori_produksi hp, aset a, hewan h, histori_hewan hh
            where a.id = h.id_aset and hp.email = '{email}' and hp.waktu_awal = hh.waktu_awal""")
    histori_produksi = namedtuplefetchall(cursor)

    return render(request, 'histori_hewan_read_pengguna.html', {'histori_produksi' : histori_produksi})

def histori_hewan_read_admin(request):

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    cursor.execute(f"""select hp.email, hp.waktu_awal, hp.waktu_selesai, hp.jumlah, hp.xp, a.nama 
            from histori_produksi hp, aset a, hewan h, histori_hewan hh
            where a.id = h.id_aset and hp.waktu_awal = hh.waktu_awal""")
    histori_produksi = namedtuplefetchall(cursor)

    return render(request, 'histori_hewan_read_admin.html', {'histori_produksi' : histori_produksi})

def histori_hewan_create(request):
    if request.method == "POST":
        email = request.session['email']
    
        cursor = connection.cursor()
        cursor.execute("set search_path to HiDay")

        jumlah = int(request.POST["jumlah"])
        id = request.POST["id_hewan"]
        cursor.execute("SELECT NOW()")
        now = cursor.fetchone()[0]
        
        cursor.execute(f"""INSERT INTO HISTORI_PRODUKSI
                VALUES('{email}', '{now}', '{now}', '{jumlah}', '{jumlah*5}')""")
        cursor.execute(f"""INSERT INTO KANDANG
                VALUES('{id}', '1', '{id}')""")
        cursor.execute(f"""INSERT INTO HEWAN
                VALUES('{id}', '{now - now}', '{id}')""")
        cursor.execute(f"""INSERT INTO HISTORI_HEWAN
                VALUES('{email}', '{now}', '{id}')""")
    else :
        cursor.execute("select * from produk_hewan")
        id_hewan = namedtuplefetchall(cursor)
        return render(request, 'histori_hewan_create_pengguna_form.html',{'id_hewan':id_hewan})
    return redirect("/histori-hewan/pengguna/create/")

def logout(request):
    request.session.flush()
    return redirect("/")

def home_pengguna(request):
    return redirect("/home/pengguna")

def home_admin(request):
    return redirect("/home/admin")

def lumbung_pengguna(request):
    return redirect("/lumbung/pengguna")

def lumbung_admin(request):
    return redirect("/lumbung/admin")               