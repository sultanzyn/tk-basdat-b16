from django.urls import path
from . import views

urlpatterns = [
    path('admin/', views.histori_hewan_read_admin, name='produksi_read_pengguna'),
    path('pengguna/read/', views.histori_hewan_read_pengguna, name='produksi_read_pengguna'),
    path('pengguna/create/', views.histori_hewan_create, name='histori_hewan_create'),
    path('admin/read/logout/', views.logout, name='produksi_read_pengguna'),
    path('pengguna/read/logout/', views.logout, name='produksi_read_pengguna'),
    path('pengguna/create/logout/', views.logout, name='histori_hewan_create'),
    path('admin/read/home/', views.home_admin, name='produksi_read_pengguna'),
    path('pengguna/read/home/', views.home_pengguna, name='produksi_read_pengguna'),
    path('pengguna/create/home/', views.home_pengguna, name='histori_hewan_create'),
    path('admin/read/lumbung/', views.lumbung_admin, name='produksi_read_pengguna'),
    path('pengguna/read/lumbung/', views.lumbung_pengguna, name='produksi_read_pengguna'),
    path('pengguna/create/lumbung/', views.lumbung_pengguna, name='histori_hewan_create'),
]
