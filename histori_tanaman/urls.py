from django.conf.urls import url
from django.urls import include,path
from . import views

urlpatterns = [
    path("create/", views.create_histori_tanaman, name="create_histori_tanaman"),
    path("read/", views.read_histori_tanaman, name="read_histori_tanaman"),
]