from unicodedata import name
from django import forms

class HistoriTanamanForm(forms.Form):
    bibit_tanaman = forms.ChoiceField(label="Bibit Tanaman")

    jumlah = forms.IntegerField(label="Jumlah")

    xp = forms.IntegerField(label="XP", disabled = True, required = False)