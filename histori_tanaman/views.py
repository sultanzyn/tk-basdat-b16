from datetime import datetime
from urllib import request, response
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

from histori_tanaman.forms import HistoriTanamanForm

# admin: R, user: CR

SET_SEARCH_PATH = 'set search_path to hiday'

def create_histori_tanaman(request):
    if 'email' not in request.session:
        return redirect('/login/')
    if request.session['tipe'] != 'Pengguna':
        return redirect('/home/')

    histori_tanaman = HistoriTanamanForm(request.POST or None)

    histori_tanaman.fields['bibit_tanaman'].choices = get_id_bibit_tanaman(request.session['email'])

    if (request.method == 'POST' and histori_tanaman.is_valid()):
        print(request.POST)

        email = request.session['email']
        waktu_awal = datetime.now()
        waktu_selesai = waktu_awal
        jumlah = histori_tanaman.cleaned_data['jumlah']
        xp = jumlah * 5
        bibit_tanaman = histori_tanaman.cleaned_data['bibit_tanaman']

        jumlah_bibit = get_jumlah_bibit_tanaman(request.session['email'])

        if (jumlah <= jumlah_bibit[bibit_tanaman]):

            insert_db_produksi(email, waktu_awal, waktu_selesai, jumlah, xp)

            insert_db_tanaman(email, waktu_awal, bibit_tanaman)

            print('Berhasil membuat histori tanaman')

            return redirect('read_histori_tanaman')

        else:
            print('Bibit tanaman tidak mencukupi')

    response = {
        'form': histori_tanaman,
    }

    return render(request, 'create_histori_tanaman.html', response)

def read_histori_tanaman(request):
    if 'email' not in request.session:
        return redirect('/login/')

    if (request.session['tipe'] == 'Admin'):

        response = {
            'historis': get_db(),
        }

        return render(request, 'read_histori_tanaman_admin.html', response)

    elif (request.session['tipe'] == 'Pengguna'):

        response = {
            'historis': get_db_for_pengguna(request.session['email']),
        }

        return render(request, 'read_histori_tanaman_pengguna.html', response)

    else:
        return redirect('/login/')


def insert_db_produksi(email, waktu_awal, waktu_selesai, jumlah, xp):
    query = f"""
    insert into histori_produksi (email, waktu_awal, waktu_selesai, jumlah, xp)
    values ('{email}', '{waktu_awal}', '{waktu_selesai}', {jumlah}, {xp})
    """
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

def insert_db_tanaman(email, waktu_awal, id_bibit_tanaman):
    query = f"""
    insert into histori_tanaman (email, waktu_awal, id_bibit_tanaman)
    values ('{email}', '{waktu_awal}', '{id_bibit_tanaman}')
    """
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

def get_db():
    # bibit_tanaman -> aset (name) + histori_produksi natural join histori_tanaman
    query = """
    SELECT *
    FROM histori_produksi hp NATURAL JOIN histori_tanaman ht, aset a
    WHERE ht.id_bibit_tanaman = a.id
    """
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def get_db_for_pengguna(email):
    query = f"""
    SELECT *
    FROM histori_produksi hp NATURAL JOIN histori_tanaman ht, aset a
    WHERE ht.id_bibit_tanaman = a.id AND email = '{email}'
    """
    
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def get_id_bibit_tanaman(email):
    query = f"""
    select a.id as id_aset, nama
    from aset a, koleksi_aset_memiliki_aset kaa natural join bibit_tanaman bt
    where a.id = bt.id_aset and kaa.id_koleksi_aset = '{email}'
        and bt.id_aset in (
            select id_bibit_tanaman
            from bibit_tanaman_ditanam_di_petak_sawah btps, koleksi_aset_memiliki_aset kaa
            where id_petak_sawah = kaa.id_aset
        )
    """
    cursor=connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)
    bibit_tanaman_response = __namedtuplefetchall(cursor)

    res=[]
    for bibit_tanaman in bibit_tanaman_response:
        res.append((bibit_tanaman.id_aset, bibit_tanaman.nama,))
    return res

def get_jumlah_bibit_tanaman(email):
    query = f"""
    select a.id as id_aset, jumlah
    from aset a, koleksi_aset_memiliki_aset kaa natural join bibit_tanaman bt
    where a.id = bt.id_aset and kaa.id_koleksi_aset = '{email}'
    """
    cursor=connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)
    bibit_tanaman_response = __namedtuplefetchall(cursor)

    res = {}
    for bibit_tanaman in bibit_tanaman_response:
        res[bibit_tanaman.id_aset] = bibit_tanaman.jumlah
    return res

def __namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]