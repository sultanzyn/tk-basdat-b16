from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import connection

# Create your views here.
def login(request):
    # Check apakah sudah logged in, bila iya redirect ke landing
    if request.session.get("email", False):
        if request.session.get("tipe") == "Admin":
            return redirect("/home/admin")
        else:
            return redirect("/home/pengguna")
    
    if request.method == "POST":
        is_found = False # Akun ditemukan
        tipe_login = None # Admin / pengguna
        password = None
        authentic = False # Password benar
        print(request)
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            
            # Check apakah Admin
            cursor.execute(f"""SELECT * FROM AKUN
                NATURAL JOIN ADMIN
                WHERE email = '{request.POST["email"]}'""")  

            row = cursor.fetchall()  
            print(row)
            if (len(row) != 0):
                is_found = True
                tipe_login='Admin'
                email = row[0][0]
                password = row[0][1]

            # Check apakah Pengguna
            cursor.execute(f"""SELECT * FROM AKUN
                NATURAL JOIN PENGGUNA
                WHERE email = '{request.POST["email"]}'""")

            row = cursor.fetchall()

            if (len(row) != 0):
                is_found = True
                tipe_login='Pengguna'  
                email = row[0][0]
                password = row[0][1]      
                request.session["nama_pertanian"] = row[0][2] 
                request.session["xp"] = row[0][3]
                request.session["koin"] = row[0][4]
                request.session["level"] = row[0][5]  

            if is_found:   
                print(password)    
                reqpass = f'{request.POST["password"]}'
                print(reqpass)
                if (password == reqpass):
                    authentic = True
                    print('sukses')
                else:
                    # Handler bila password salah
                    messages.add_message(request, messages.WARNING, f"Password salah, silahkan coba lagi")

            # Handler bila berhasil login
            if authentic:
                cursor.execute("set search_path to public")
                request.session["email"] = email
                request.session["tipe"] = tipe_login
                if tipe_login == 'Admin':
                    return redirect("/home/admin")
                else:
                    return redirect("/home/pengguna")
            
            # Handler bila tidak ditemukan emailnya
            if (not is_found):
                messages.add_message(request, messages.WARNING, f"Login gagal, email tidak ditemukan silahkan coba lagi")
            
            cursor.execute("set search_path to public")
    return render(request, 'login.html')

def logout(request):
    # Hilangkan semua dari session
    request.session.flush()

    return redirect("../../")
