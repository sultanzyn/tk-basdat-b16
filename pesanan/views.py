from urllib import request
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def pengguna(request):

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    cursor.execute("select * from pesanan")
    pesanan = namedtuplefetchall(cursor)

    return render(request, 'pesanan_pengguna.html', {'pesanan' : pesanan}) 


def admin_read(request):

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    cursor.execute("select * from pesanan")
    pesanan = namedtuplefetchall(cursor)

    return render(request, 'pesanan_admin.html', {'pesanan' : pesanan}) 

#detail pesanan blm kelar
def admin_create(request):

    if request.method == "POST":
        email = request.session['email']
    
        cursor = connection.cursor()
        cursor.execute("set search_path to HiDay")

        id = request.POST["id_pesanan"]
        nama = request.POST["nama_pesanan"]
        jenis = request.POST["jenis_pesanan"]
        jumlah = request.POST["jumlah"]
        count = int(request.POST["count"])

        cursor.execute(f"""insert into pesanan values('{id}', 'Baru Dipesan', '{jenis}', '{nama}', '0')""")
        for i in count: {
            cursor.execute(f"""insert into detail_pesanan values('{id}', 'i'+'1', '{jenis}', '{jumlah}', '0')""")
        }
        

    else :
        cursor = connection.cursor()
        cursor.execute("set search_path to HiDay")

        cursor.execute("select * from produk")
        produk = namedtuplefetchall(cursor)

        return render(request, 'buat_pesanan.html', {'produk' : produk})

    return redirect("/pesanan/admin/create/")
        

def logout(request):
    # Hilangkan semua dari session
    request.session.flush()

    return redirect("/")

def lumbung_pengguna(request):
    return redirect("/lumbung/pengguna")

def lumbung_admin(request):
    return redirect("/lumbung/admin")    