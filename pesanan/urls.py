from django.urls import path
from . import views

urlpatterns = [
    path('pengguna/', views.pengguna),
    path('admin/read/', views.admin_read),
    path('admin/create/', views.admin_create),
    # path('admin/update/', views.admin),
    # path('admin/delete/', views.admin),
    path('pengguna/logout/', views.logout),
    path('admin/read/logout/', views.logout),
    path('admin/create/logout/', views.logout),
]