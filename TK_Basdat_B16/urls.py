"""TK_Basdat_B16 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('produk/', include('produk.urls')),
    path('produksi/', include('produksi.urls')),
    path('histori-hewan/', include('histori_hewan.urls')),
    path('register/', include('registration_page.urls')),
    path('login/', include('login_page.urls')),
    path('home/', include('homepage.urls')),
    path('paket-koin/', include('paket_koin.urls')),
    path('transaksi-pembelian-koin/', include('transaksi_pembelian_koin.urls')),
    path('transaksi-upgrade-lumbung/', include('transaksi_upgrade_lumbung.urls')),
    path('histori-tanaman/', include('histori_tanaman.urls')),
    path('aset/', include('aset_page.urls')),
    path('koleksi-aset/', include('koleksi_aset.urls')),
    path('lumbung/', include('lihat_isi_lumbung.urls')),
    path('histori-makanan/', include('histori_makanan.urls')),
    path('histori-penjualan/', include('histori_penjualan.urls')),
    path('pesanan/', include('pesanan.urls')),
]
