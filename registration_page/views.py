from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.contrib import messages

def register(request):

    return render(request, 'register.html')

def admin(request):
    # Check apakah sudah logged in, bila iya redirect ke landing
    # if request.session.get("email", False):
    #     if request.session.get("tipe") == "Admin":
    #         return redirect("/home/admin")
    #     else:
    #         return redirect("/home/pengguna")
    
    if request.method == "POST":
        tipe_login = None # Admin / pengguna
        password = None
        email = request.POST["email"]
        password = request.POST["password"]
        print(request)
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            
            # Check apakah Admin
            cursor.execute(f"""INSERT INTO AKUN
                VALUES('{email}')""")
            cursor.execute(f"""INSERT INTO ADMIN
                VALUES('{email}', '{password}')""")

            tipe_login='Admin'

            # Handler bila berhasil register
            cursor.execute("set search_path to public")
            request.session["email"] = email
            request.session["tipe"] = tipe_login
            cursor.execute("set search_path to public")
            return redirect("/home/admin")
    return render(request, 'admin.html')

def pengguna(request):

    # if request.session.get("email", False):
    #     if request.session.get("tipe") == "Admin":
    #         return redirect("/home/admin")
    #     else:
    #         return redirect("/home/pengguna")
    
    if request.method == "POST":
        tipe_login = None # Admin / pengguna
        email = request.POST["email"]
        password = request.POST["password"]
        nama_area_pertanian = request.POST['nama_area_pertanian']
        print(request)
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            
            # Check apakah PENGGUNA
            cursor.execute("set search_path to HiDay;")
            cursor.execute(f"""INSERT INTO AKUN
                VALUES('{email}')""")
            cursor.execute(f"""INSERT INTO PENGGUNA
                VALUES('{email}', '{password}', '{nama_area_pertanian}')""")
            tipe_login='Pengguna'

            # Handler bila berhasil register
            cursor.execute("set search_path to public")
            request.session["email"] = email
            request.session["tipe"] = tipe_login
            cursor.execute("set search_path to public")
            return redirect("/home/pengguna")
    return render(request, 'pengguna.html')

def namedtuplefetchall(cursor):
    
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]