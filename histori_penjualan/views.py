from copy import error
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.contrib import messages

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def histori_penjualan_read_pengguna(request):

    email = request.session.get('email')

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    cursor.execute(f"""select * from histori_penjualan where email = '{email}'""")
    histori_produksi = namedtuplefetchall(cursor)

    return render(request, 'histori_penjualan_read_pengguna.html', {'histori_produksi' : histori_produksi})

def histori_penjualan_read_admin(request):

    cursor = connection.cursor()
    cursor.execute("set search_path to HiDay")

    cursor.execute("select * from histori_penjualan")
    histori_produksi = namedtuplefetchall(cursor)

    return render(request, 'histori_penjualan_read_admin.html', {'histori_produksi' : histori_produksi})

def logout(request):
    request.session.flush()
    return redirect("/")

def home_pengguna(request):
    return redirect("/home/pengguna")

def home_admin(request):
    return redirect("/home/admin")

def lumbung_pengguna(request):
    return redirect("/lumbung/pengguna")

def lumbung_admin(request):
    return redirect("/lumbung/admin")               