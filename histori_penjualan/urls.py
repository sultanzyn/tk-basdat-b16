from django.urls import path
from . import views

urlpatterns = [
    path('admin/', views.histori_penjualan_read_admin, name='produksi_read_admin'),
    path('pengguna/', views.histori_penjualan_read_pengguna, name='produksi_read_pengguna'),
    path('admin/logout/', views.logout, name='produksi_read_pengguna'),
    path('pengguna/logout/', views.logout, name='produksi_read_pengguna'),
    path('admin/home/', views.home_admin, name='produksi_read_pengguna'),
    path('pengguna/home/', views.home_pengguna, name='produksi_read_pengguna'),
    path('admin/read/lumbung/', views.lumbung_admin, name='produksi_read_pengguna'),
    path('pengguna/lumbung/', views.lumbung_pengguna, name='produksi_read_pengguna'),
]
