from django.conf.urls import url
from django.urls import include,path
from . import views

urlpatterns = [
    path("buy/<id>", views.create_transaksi_pembelian_koin, name="create_transaksi_pembelian_koin"),
    path("read/", views.read_transaksi_pembelian_koin, name="read_transaksi_pembelian_koin"),
]