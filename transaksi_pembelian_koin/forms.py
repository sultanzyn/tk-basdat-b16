from unicodedata import name
from django import forms

class TransaksiPembelianKoinForm(forms.Form):
    paket_koin = forms.IntegerField(label="Paket Koin")

    harga = forms.IntegerField(label="Harga")

    jumlah = forms.IntegerField(label="Jumlah")

    cara_pembayaran = forms.CharField(label="Cara Pembayaran", max_length=15)