from datetime import datetime
from urllib import request
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

from transaksi_pembelian_koin.forms import TransaksiPembelianKoinForm

# admin: R, user: CR

SET_SEARCH_PATH = 'set search_path to hiday'

def create_transaksi_pembelian_koin(request, id):
    if 'email' not in request.session:
        return redirect('/login/')
    if request.session['tipe'] != 'Pengguna':
        return redirect('/home/')
    
    prefilled_data = get_paket_koin_by_pk(id)

    if (prefilled_data != None):
        response={}
        response['error']=False

        form = TransaksiPembelianKoinForm(request.POST or None,
            initial={'paket_koin': id,
                'harga': prefilled_data.harga})

        form.fields['paket_koin'].disabled = True
        form.fields['harga'].disabled = True
        
        response['form'] = form
        response['id'] = id

    if (request.method == "POST" and form.is_valid()):
            print(request.POST)
            email = request.session['email']
            waktu = datetime.now()
            jumlah = form.cleaned_data['jumlah']
            cara_pembayaran = form.cleaned_data['cara_pembayaran']
            paket_koin = form.cleaned_data['paket_koin']
            total_biaya = jumlah * paket_koin

            if (total_biaya < 0):
                total_biaya = 0

            insert_db(email, waktu, jumlah, cara_pembayaran, paket_koin, total_biaya)
            print('Berhasil membeli paket koin')

            return redirect("read_transaksi_pembelian_koin")
 
    return render(request, "create_transaksi_pembelian_koin.html", response)

def read_transaksi_pembelian_koin(request):
    if 'email' not in request.session:
        return redirect('/login/')

    if (request.session['tipe'] == 'Admin'):

        response = {
            'transaksis': get_db(),
        }

        return render(request, 'read_transaksi_pembelian_koin_admin.html', response)

    elif (request.session['tipe'] == 'Pengguna'):

        response = {
            'transaksis': get_db_for_pengguna(request.session['email']),
        }

        return render(request, 'read_transaksi_pembelian_koin_pengguna.html', response)

    else:
        return redirect('/login/')

        
def insert_db(email, waktu, jumlah, cara_pembayaran, paket_koin, total_biaya):
    query = f"""
    INSERT INTO transaksi_pembelian_koin
    (email, waktu, jumlah, cara_pembayaran, paket_koin, total_biaya)
    VALUES ('{email}', '{waktu}', {jumlah}, '{cara_pembayaran}', {paket_koin}, {total_biaya})
    """

    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

def get_db():
    query = """
    SELECT * FROM transaksi_pembelian_koin
    """
    
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def get_paket_koin_by_pk(jumlah_koin):
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(f"select * from paket_koin where jumlah_koin = {jumlah_koin}")

    return __namedtuplefetchall(cursor)[0]

def get_db_for_pengguna(email):
    query = f"""
    SELECT * FROM transaksi_pembelian_koin where email = '{email}'
    """

    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def __namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]