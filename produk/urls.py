from django.urls import path
from . import views

urlpatterns = [
    path('admin/read/', views.produk_read_admin, name='produk_read_pengguna'),
    path('pengguna/read/', views.produk_read_pengguna, name='produk_read_pengguna'),
    path('admin/create/', views.produk_create, name='produk_create'),
    path('update/<str:id_produk>/<str:nama>/<str:jenis_produk>', views.produk_update, name='produk_update'),
    path('hapus/<str:id_produk>/<str:jenis_produk>', views.hapusProduk, name='hapusProduk'),
]
