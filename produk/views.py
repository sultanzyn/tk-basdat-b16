from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple

# Create your views here.
no_ph = 5
no_pm = 9
no_hp = 11
def produk_read_pengguna(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT * 
            FROM PRODUK""") 
        row = cursor.fetchall()
        row_jenis = []
        result = [[]]
        # print(len(row))
        # print(len(row_jenis))
        # print(row[0][0])
        for i in row:
            cursor.execute(f"""SELECT * 
                FROM PRODUK_HEWAN
                WHERE id_produk = '{i[0]}'""")
            ph = namedtuplefetchall(cursor)
            cursor.execute(f"""SELECT * 
                FROM PRODUK_MAKANAN
                WHERE id_produk = '{i[0]}'""")
            pm = namedtuplefetchall(cursor)
            cursor.execute(f"""SELECT * 
                FROM HASIL_PANEN
                WHERE id_produk = '{i[0]}'""")
            hp = namedtuplefetchall(cursor)
            if (len(ph) != 0):
                row_jenis.append('Produk Hewan')
                # print('ph')
            elif (len(pm) != 0):
                row_jenis.append('Produk Makanan')
                # print('pm')
            elif (len(hp) != 0):
                row_jenis.append('Hasil Panen')
                # print('hp')
            else:
                row_jenis.append('')
        # print(len(row_jenis))
        counter1 = 0
        counter2 = 0

        for i in row:
            for j in i:
                result[counter1].append(j)
            result[counter1].append(row_jenis[counter1])
            result.append([])
            counter1 += 1

        context = {
            'data' : result,
        }
    return render(request, 'produk_read_pengguna.html', context)

def produk_read_admin(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT * 
            FROM PRODUK""") 
        row = cursor.fetchall()
        row_jenis = []
        result = [[]]
        # print(len(row))
        # print(len(row_jenis))
        # print(row[0][0])
        # cursor.execute(f"""SELECT * 
        #     FROM PRODUK_HEWAN""")
        # test = cursor.fetchall()
        # stringTest = str(test[len(test) - 1])
        # for i in stringTest:
        #     if i.isdigit():
        #         print(i)
        for i in row:
            cursor.execute(f"""SELECT * 
                FROM PRODUK_HEWAN
                WHERE id_produk = '{i[0]}'""")
            ph = namedtuplefetchall(cursor)
            cursor.execute(f"""SELECT * 
                FROM PRODUK_MAKANAN
                WHERE id_produk = '{i[0]}'""")
            pm = namedtuplefetchall(cursor)
            cursor.execute(f"""SELECT * 
                FROM HASIL_PANEN
                WHERE id_produk = '{i[0]}'""")
            hp = namedtuplefetchall(cursor)
            if (len(ph) != 0):
                row_jenis.append('Produk Hewan')
                # print('ph')
            elif (len(pm) != 0):
                row_jenis.append('Produk Makanan')
                # print('pm')
            elif (len(hp) != 0):
                row_jenis.append('Hasil Panen')
                # print('hp')
            else:
                row_jenis.append('')
        # print(len(row_jenis))
        counter1 = 0
        counter2 = 0

        for i in row:
            for j in i:
                result[counter1].append(j)
            result[counter1].append(row_jenis[counter1])
            if counter1 < (len(row)-1):
                result.append([])
                counter1 += 1

        cursor.execute(f"""select id from demi_delete_1
            union
            select id from demi_delete_2
            union
            select id from demi_delete_3
            union
            select id from demi_delete_4
            union
            select id from demi_delete_5
            union
            select id from demi_delete_6
            union
            select id from demi_delete_7
            order by id
            """) 

        delete_data = cursor.fetchall()
        delete_data_final = []
        for i in delete_data:
            delete_data_final.append(i[0])
        # print(delete_data_final)
        context = {
            'data' : result,
            'delete': delete_data_final,
        }
    return render(request, 'produk_read_admin.html', context)

def produk_create(request):
    email = request.session.get('email')
    if request.method == "POST":
        jenis_produk = request.POST['jenis_produk']
        nama = request.POST['nama']
        harga_jual = request.POST['harga_jual']
        sifat_produk = request.POST['sifat_produk']
        _id = ''
        print(request)
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            
            cursor.execute(f"""SELECT * FROM {jenis_produk}""")

            jumlah = cursor.fetchall()
            if (jenis_produk == 'produk_hewan'):
                terakhir = str(jumlah[len(jumlah)-1])
                numLast = []
                for i in terakhir:
                    if i.isdigit():
                        numLast.append(i)
                angka = int(''.join(numLast))
                _id = 'PH' + str(angka + 1)
            elif (jenis_produk == 'hasil_panen'):
                terakhir = str(jumlah[len(jumlah)-1])
                numLast = []
                for i in terakhir:
                    if i.isdigit():
                        numLast.append(i)
                angka = int(''.join(numLast))
                _id = 'HP' + str(angka + 1)
            else:
                terakhir = str(jumlah[len(jumlah)-1])
                numLast = []
                for i in terakhir:
                    if i.isdigit():
                        numLast.append(i)
                angka = int(''.join(numLast))
                _id = 'PM' + str(angka + 1)

            cursor.execute(f"""INSERT INTO PRODUK
                VALUES('{_id}', '{nama}', '{harga_jual}', '{sifat_produk}')""")
            cursor.execute(f"""INSERT INTO {jenis_produk}
                VALUES('{_id}')""")

            # Handler bila berhasil register
            cursor.execute("set search_path to public")
            return redirect('../read/')
    return render(request, 'produk_create_form.html')

def produk_update(request, id_produk, nama, jenis_produk):
    email = request.session.get('email')
    # print(nama)
    # print(id_produk)
    # print(jenis_produk)
    if request.method == "POST":
        harga_jual = request.POST['harga_jual']
        sifat_produk = request.POST['sifat_produk']
        _id = id_produk
        print(request)
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            
            cursor.execute(f"""UPDATE PRODUK
            SET harga_jual = '{harga_jual}', sifat_produk = '{sifat_produk}'
            WHERE id = '{_id}'""")

            # Handler bila berhasil register
            cursor.execute("set search_path to public")
            return redirect('../../../admin/read/')
    context = {
        'id': id_produk,
        'nama': nama,
        'jenis_produk': jenis_produk,
    }
    return render(request, 'produk_update_form.html', context)

def hapusProduk(request, id_produk, jenis_produk):
    email = request.session.get('email')
    with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            if jenis_produk == "Produk Hewan":
                cursor.execute(f"""DELETE FROM produk_hewan
                    WHERE id_produk = '{id_produk}'""")
            elif jenis_produk == "Produk Makanan":
                cursor.execute(f"""DELETE FROM produk_makanan
                WHERE id_produk = '{id_produk}'""")
            else:
                cursor.execute(f"""DELETE FROM hasil_panen
                WHERE id_produk = '{id_produk}'""")
            cursor.execute(f"""DELETE FROM PRODUK
                WHERE id = '{id_produk}'""")
    return redirect('../../admin/read/')

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]