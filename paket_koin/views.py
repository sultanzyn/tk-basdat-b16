import json
from urllib import request, response
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.db import connection
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt

from paket_koin.forms import PaketKoinForm

# admin: CRUD, user: R

SET_SEARCH_PATH = 'set search_path to hiday'

def create_paket_koin(request):
    if 'email' not in request.session:
        return redirect('/login/')
    if request.session['tipe'] != 'Admin':
        return redirect('/home/')

    paket_koin = PaketKoinForm(request.POST or None)

    if (request.method == 'POST' and paket_koin.is_valid()):
        print(request.POST)

        jumlah_koin = paket_koin.cleaned_data['jumlah_koin']
        harga = paket_koin.cleaned_data['harga']

        try: 
            insert_db(jumlah_koin, harga)
            print('Berhasil create paket_data')
            return redirect('read_paket_koin')
        except:
            print('Gagal create paket_data')

    response = {
        'form': paket_koin
    }

    return render(request, 'create_paket_koin.html', response)

def read_paket_koin(request):
    if 'email' not in request.session:
        return redirect('/login/')

    response = {
        'paket_koins': get_db_for_read(),
        'tipe': request.session['tipe'],
    }

    return render(request, 'read_paket_koin.html', response)

@csrf_exempt
def update_paket_koin(request, id):
    if 'email' not in request.session:
        return redirect('/login/')
    if request.session['tipe'] != 'Admin':
        return redirect('/home/')

    prefilled_data = get_db_by_pk(id)

    if (prefilled_data != None):

        form = PaketKoinForm(request.POST or None, initial={
            'jumlah_koin': id,
            'harga': prefilled_data.harga
        })

        response={}
        response['error']=False    
        form.fields['jumlah_koin'].disabled = True
        response['form'] = form
        response['id'] = id
 
    if(request.method == "POST" and form.is_valid()):
        print(request.POST)

        jumlah_koin = form.cleaned_data['jumlah_koin']
        harga = form.cleaned_data['harga']

        update_db(jumlah_koin, harga)
        print('Berhasil update paket koin')

        return redirect("read_paket_koin")
 
    return render(request, "update_paket_koin.html", response)


@csrf_exempt
def delete_paket_koin(request):
    if 'email' not in request.session:
        return redirect('/login/')
    if request.session['tipe'] != 'Admin':
        return redirect('/home/')
    
    if(request.method == "POST"):
        print(request.POST)

        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        delete_db(body['payload'])
        print('Berhasil delete paket koins')
        
        return redirect("read_paket_koin")
    return HttpResponse("Nothing here")


def delete_db(pk_paket_koin):
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(f"delete from paket_koin where jumlah_koin = {pk_paket_koin};")

def insert_db(jumlah_koin, harga):
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(f"insert into paket_koin (jumlah_koin, harga) values ({jumlah_koin}, {harga});")

def update_db(jumlah_koin, harga):
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(f"update paket_koin set harga={harga} where jumlah_koin={jumlah_koin};")

def get_db_for_read():
    query = """
    SELECT DISTINCT pk.jumlah_koin, pk.harga
    FROM paket_koin pk LEFT OUTER JOIN transaksi_pembelian_koin tp
        ON pk.jumlah_koin = tp.paket_koin;
    """

    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(query)

    return __namedtuplefetchall(cursor)

def get_db_by_pk(jumlah_koin):
    cursor = connection.cursor()
    cursor.execute(SET_SEARCH_PATH)
    cursor.execute(f"select * from paket_koin where jumlah_koin = {jumlah_koin};")

    return __namedtuplefetchall(cursor)[0]

def __namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]