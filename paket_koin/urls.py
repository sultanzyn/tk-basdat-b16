from django.conf.urls import url
from django.urls import include,path
from . import views

urlpatterns = [
    path("create/", views.create_paket_koin, name="create_paket_koin"),
    path("read/", views.read_paket_koin, name="read_paket_koin"),
    path("update/<id>", views.update_paket_koin, name="update_paket_koin"),
    path("delete/", views.delete_paket_koin, name="delete_paket_koin"),
]
