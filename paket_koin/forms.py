from unicodedata import name
from django import forms

class PaketKoinForm(forms.Form):
    jumlah_koin = forms.IntegerField(label="Jumlah Koin")

    harga = forms.IntegerField(label="Harga")