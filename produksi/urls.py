from django.urls import path
from . import views

urlpatterns = [
    path('admin/read/', views.produksi_read_admin, name='produksi_read_pengguna'),
    path('pengguna/read/', views.produksi_read_pengguna, name='produksi_read_pengguna'),
    path('admin/create/', views.produksi_create, name='produksi_create'),
    path('update/<str:nama_produk_makanan>/<str:nama_aset>/<str:tupleDua>', views.produksi_update, name='produksi_update'),
    path('detail/<str:nama_produk_makanan>/<str:nama_aset>/<str:durasi>/<str:jumlah_dihasilkan>/<str:tupleDua>', views.produksi_detail, name='produksi_detail'),
    path('hapus/<str:tupleDua>', views.hapusProduksi, name='hapusProduksi'),
]
