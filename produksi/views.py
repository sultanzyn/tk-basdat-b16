from django.shortcuts import render, redirect
from collections import namedtuple
from django.db import connection

# Create your views here.
def produksi_read_pengguna(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")
        hasil = [[]]
        counter_hasil = 0
        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT P.nama, A.nama, PROD.durasi, PROD.jumlah_unit_hasil, A.id, P.id
            FROM PRODUKSI PROD, PRODUK P, ASET A
            WHERE PROD.id_produk_makanan = P.id AND PROD.id_alat_produksi = A.id
            """) 
        row = cursor.fetchall()
        for i in row:
            hasil[counter_hasil].append(i[0])
            hasil[counter_hasil].append(i[1])
            hasil[counter_hasil].append(i[2])
            hasil[counter_hasil].append(i[3])
            newtuple = (i[4],i[5])
            hasil[counter_hasil].append(newtuple)
            hasil.append([])
            counter_hasil+=1
        hasil.pop()
        context = {
            'data' : hasil,
        }
    return render(request, 'produksi_read_pengguna.html', context)

def produksi_read_admin(request):
    email = request.session.get('email')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")
        hasil = [[]]
        counter_hasil = 0
        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT P.nama, A.nama, PROD.durasi, PROD.jumlah_unit_hasil, A.id, P.id
            FROM PRODUKSI PROD, PRODUK P, ASET A
            WHERE PROD.id_produk_makanan = P.id AND PROD.id_alat_produksi = A.id
            """) 
        row = cursor.fetchall()
        for i in row:
            hasil[counter_hasil].append(i[0])
            hasil[counter_hasil].append(i[1])
            hasil[counter_hasil].append(i[2])
            hasil[counter_hasil].append(i[3])
            newtuple = (i[4],i[5])
            hasil[counter_hasil].append(newtuple)
            hasil.append([])
            counter_hasil+=1
        hasil.pop()
        cursor.execute(f"""SELECT id_alat_produksi, id_produk_makanan
            FROM HISTORI_PRODUKSI_MAKANAN
            """)
        row2 = cursor.fetchall()
        print(hasil)
        print(row2)
        context = {
            'data' : hasil,
            'delete': row2,
        }
    return render(request, 'produksi_read_admin.html', context)

def produksi_create(request):
    email = request.session.get('email')
    if request.method == "POST":
        counter_bahan = 0
        id_produk_makanan = request.POST['produk_makanan']
        id_alat_produksi = request.POST['alat_produksi']
        durasi = request.POST['durasi']
        durasi_jam = int(durasi)//60
        if durasi_jam == 0:
            durasi_jam = "00"
        durasi_menit = int(durasi)%60
        time = str(durasi_jam) + ":" + str(durasi_menit) + ":" + "00"
        jumlah_dihasilkan = request.POST['jumlah_produk_dihasilkan']
        bahan = request.POST.getlist('bahan') #list
        jumlah_bahan = request.POST.getlist('jumlah_produk_dibutuhkan') #list
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
    
            # Mengirim atribut pengguna
            cursor.execute(f"""INSERT INTO PRODUKSI
                VALUES('{id_alat_produksi}', '{id_produk_makanan}', '{time}', '{jumlah_dihasilkan}')""")
            counter_bahan = 0
            for i in range(len(bahan)):
                cursor.execute(f"""INSERT INTO PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN
                    VALUES('{id_produk_makanan}', '{bahan[counter_bahan]}', '{jumlah_bahan[counter_bahan]}')""")
                counter_bahan+=1

            # Handler bila berhasil register
            cursor.execute("set search_path to public")
            return produksi_read_admin(request)
        # print(request)
        # print(request.POST)
        print(request.POST.getlist('bahan'))
        print(request.POST.getlist('jumlah_produk_dibutuhkan'))
        # with connection.cursor() as cursor:
        #     cursor.execute("set search_path to HiDay")
            
        #     cursor.execute(f"""SELECT * FROM {jenis_produk}""")

        #     jumlah = cursor.fetchall()
        #     if (jenis_produk == 'produk_hewan'):
        #         _id = 'PH' + str(len(jumlah) + 1) 
        #     elif (jenis_produk == 'hasil_panen'):
        #         _id = 'HP' + str(len(jumlah) + 1)
        #     else:
        #         _id = 'PM' + str(len(jumlah) + 1)

        #     cursor.execute(f"""INSERT INTO PRODUK
        #         VALUES('{_id}', '{nama}', '{harga_jual}', '{sifat_produk}')""")
        #     cursor.execute(f"""INSERT INTO {jenis_produk}
        #         VALUES('{_id}')""")

        #     # Handler bila berhasil register
        #     cursor.execute("set search_path to public")
        #     return produksi_read_admin(request)
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        # Mengirim atribut pengguna
        cursor.execute(f"""SELECT * FROM PRODUK
                INNER JOIN PRODUK_MAKANAN
                ON id = id_produk""") 
        row = namedtuplefetchall(cursor)
        # print(row)
        cursor.execute(f"""SELECT * 
            FROM ASET INNER JOIN ALAT_PRODUKSI
            ON ID=ID_ASET""")
        row2 = namedtuplefetchall(cursor)
        cursor.execute(f"""SELECT * 
            FROM PRODUK""")
        row3 = namedtuplefetchall(cursor)
        context = {
            'produk_makanan' : row,
            'alat_produksi' : row2,
            'produk' : row3,
        }
    return render(request, 'produksi_create_form.html', context)

def produksi_update(request, nama_produk_makanan, nama_aset, tupleDua):
    rilTuple = eval(tupleDua)
    aset_id = rilTuple[0]
    produk_id = rilTuple[1]
    # for i in rilTuple:
    #     print(i)
    row = None
    if request.method == "POST":
        durasi = request.POST['durasi']
        durasi_jam = int(durasi)//60
        if durasi_jam == 0:
            durasi_jam = "00"
        durasi_menit = int(durasi)%60
        time = str(durasi_jam) + ":" + str(durasi_menit) + ":" + "00"
        jumlah_dihasilkan = request.POST['jumlah_hasil']
        print(request)
        with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            
            cursor.execute(f"""UPDATE PRODUKSI
            SET durasi = '{time}', jumlah_unit_hasil = '{jumlah_dihasilkan}'
            WHERE id_alat_produksi = '{aset_id}' AND id_produk_makanan = '{produk_id}'""")

            # Handler bila berhasil register
            cursor.execute("set search_path to public")
            return redirect('../../../admin/read/')
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        cursor.execute(f"""SELECT PROD.nama, PB.jumlah
                FROM PRODUK PROD, PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN PB
                WHERE PROD.id = PB.id_produk AND PB.id_produk_makanan = '{produk_id}'
                """)
        row = cursor.fetchall()
    context = {
        'nama_makanan' : nama_produk_makanan,
        'nama_alat' : nama_aset,
        'data' : row,
    }
    return render(request, 'produksi_update_form.html', context)

def produksi_detail(request, nama_produk_makanan, nama_aset, durasi, jumlah_dihasilkan, tupleDua):
    waktu2 = durasi.split(':')
    jam = int(waktu2[0]) * 60
    menit = int(waktu2[1])
    total = str(jam + menit)
    # print(eval(tupleDua))
    rilTuple = eval(tupleDua)
    aset_id = rilTuple[0]
    produk_id = rilTuple[1]
    # for i in rilTuple:
    #     print(i)
    row = None
    with connection.cursor() as cursor:
        cursor.execute("set search_path to HiDay")

        cursor.execute(f"""SELECT PROD.nama, PB.jumlah
                FROM PRODUK PROD, PRODUK_DIBUTUHKAN_OLEH_PRODUK_MAKANAN PB
                WHERE PROD.id = PB.id_produk AND PB.id_produk_makanan = '{produk_id}'
                """)
        row = cursor.fetchall()
    context = {
        'nama_makanan' : nama_produk_makanan,
        'nama_alat' : nama_aset,
        'durasi' : total,
        'jumlah_hasil' : jumlah_dihasilkan,
        'data' : row,
    }
    return render(request, 'produksi_detail.html', context)

def hapusProduksi(request, tupleDua):
    email = request.session.get('email')
    rilTuple = eval(tupleDua)
    aset_id = rilTuple[0]
    produk_id = rilTuple[1]
    with connection.cursor() as cursor:
            cursor.execute("set search_path to HiDay")
            cursor.execute(f"""DELETE FROM PRODUKSI
                    WHERE id_alat_produksi = '{aset_id}' AND id_produk_makanan = '{produk_id}'""")
    return redirect('../admin/read/')

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]